package id.co.knt.bikereview.service;

import id.co.knt.bikereview.domain.Bicycle;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

public class BicycleServiceInMemory implements BicycleService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2791952504974245741L;

	private List<Bicycle> bicycles = new ArrayList<Bicycle>();
	
	/* (non-Javadoc)
	 * @see id.co.knt.bikereview.service.BicycleService#init()
	 */
	@PostConstruct
	public void init() {
		bicycles.add(new Bicycle("Giant", "Anthem", "2010"));
		bicycles.add(new Bicycle("Specialized", "Allez", "2011"));
		bicycles.add(new Bicycle("Polygon", "Thunder 300", "2009"));
	}
	
	/* (non-Javadoc)
	 * @see id.co.knt.bikereview.service.BicycleService#findAll()
	 */
	@Override
	public List<Bicycle> findAll() {
		return bicycles;
	}
	
	/* (non-Javadoc)
	 * @see id.co.knt.bikereview.service.BicycleService#save(id.co.knt.bikereview.domain.Bicycle)
	 */
	@Override
	public void save(Bicycle b) {
		boolean exists = false;
		for (Bicycle bicycle:bicycles) {
			if (bicycle == b) {
				exists = true;
				break;
			}
		}
		if (!exists)
			bicycles.add(b);
	}
	
	/* (non-Javadoc)
	 * @see id.co.knt.bikereview.service.BicycleService#delete(id.co.knt.bikereview.domain.Bicycle)
	 */
	@Override
	public void delete(Bicycle b) {
		bicycles.remove(b);
	}
}
