package id.co.knt.bikereview.web;

import id.co.knt.bikereview.domain.Bicycle;
import id.co.knt.bikereview.service.BicycleService;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ManagedBean
@SessionScoped
public class BicycleBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2334706688188154338L;

	@ManagedProperty(value="#{bicycleService}")
	private BicycleService bicycleService;
	
	private List<Bicycle> bicycles;
	private Bicycle selectedBike;
	
	private boolean dataVisible = false;
	private boolean formVisible = false;
	
	private static Logger log = LoggerFactory.getLogger("main");
	
	@PostConstruct
	public void init() {
		System.out.println("init called");
		findAll();
		reset();
	}
	
	public String findAll() {
		bicycles = bicycleService.findAll();
		dataVisible = !bicycles.isEmpty();
		
		return null;
	}
	
	public void reset() {
		log.info("reset called");
		formVisible = false;
		selectedBike = new Bicycle();
	}
	
	public BicycleService getBicycleService() {
		return bicycleService;
	}
	public void setBicycleService(BicycleService bicycleService) {
		this.bicycleService = bicycleService;
	}
	public List<Bicycle> getBicycles() {
		return bicycles;
	}
	public void setBicycles(List<Bicycle> bicycles) {
		this.bicycles = bicycles;
	}
	public Bicycle getSelectedBike() {
		return selectedBike;
	}
	public void setSelectedBike(Bicycle selectedBike) {
		this.selectedBike = selectedBike;
	}
	public boolean isDataVisible() {
		return dataVisible;
	}
	public void setDataVisible(boolean dataVisible) {
		this.dataVisible = dataVisible;
	}
	public boolean isFormVisible() {
		return formVisible;
	}
	public void setFormVisible(boolean formVisible) {
		this.formVisible = formVisible;
	}
	
	public String persist() {
		String message = "Saved!";
		bicycleService.save(selectedBike);
		
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("bikeEditor.hide()");
		
		FacesMessage facesMessage = new FacesMessage(message);
		FacesContext.getCurrentInstance().addMessage(null, facesMessage);
		
		reset();
		return findAll();
	}
	
	public String delete() {
		String message = "deleted!";
		bicycleService.delete(selectedBike);
		
		FacesMessage facesMessage = new FacesMessage(message);
		FacesContext.getCurrentInstance().addMessage(null, facesMessage);
		
		reset();
		return findAll();
	}
	
	public String displayEditor() {
		formVisible=true;
		log.info("displayEditor called, formVisible : " + formVisible);
		return null;
	}
	
	public void handleDialogClose(CloseEvent e) {
		System.out.println("handleDialogClose called, formVisible : " + formVisible);
		reset();
		System.out.println("handleDialogClose finished, formVisible : " + formVisible);
	}
}
